import React, { Component } from 'react';

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: this.props.flag
    };
  }

  componentWillReceiveProps(nextProps, nextContext){
    if(nextProps.flag !== this.state.isOpen){
      this.setState({isOpen: nextProps.flag})
    }
  }

  handleClick = (val) => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render = () => {
    const data = (
      <p>{this.props.post.data}</p>
    );
    const description = (
      <p className="description">{this.props.post.description}</p>
    );
    return (
      <React.Fragment>
        <div className="post">
          <h1 className={this.state.isOpen ? "news" : "newsMini"}>{this.props.post.name}</h1>
          <img className={this.state.isOpen ? "img" : "imgMini"} src={this.props.post.img} alt={this.props.post.name} />
          {this.state.isOpen ? data : description}
          <button className="buttonInPost" onClick={this.handleClick}>{this.state.isOpen ? "Свернуть" : "Развернуть"}</button>
        </div>
      </React.Fragment>
    )
  }
}

export default Post;

