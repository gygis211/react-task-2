import React from 'react';
import News from "./News";

function App() {
    return (
      <div className = "newsRoot">
        <News />
      </div>
    )
}

export default App;
