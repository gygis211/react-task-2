import React, {Component} from 'react';
import Post from "./Post";
import newsList from './newsList';

class News extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            news: newsList,
            value: "",
        }

    }

    
    handleChange = (event) => {
        this.setState({ value: event.target.value });
    };
    
    revers = () => {
        this.setState({
        news: this.state.news.reverse(),
        });
    };
    
    findNews = () => {
        this.setState({
        news: this.state.news.filter((el) => {
            return el.name === this.state.value;
        }),
        });
    };
    
    reloadNews = () => {
        this.setState ({
            news: newsList,
        })
    };
    

    render() {
        return (
            <React.Fragment>
                <h1>Task 13 - News</h1>
                <button onClick={this.revers}>Reverse</button>
                <input
                    type="text"
                    value={this.state.value}
                    onChange={this.handleChange}
                />
                <button onClick={this.findNews}>Find</button>
                <button onClick={this.reloadNews}>Reload</button>
                {this.state.news.map((post, i) => i===0 ? <Post key={post.id} post={post} flag/> : <Post key={post.id} post={post} />)}
            </React.Fragment>
        )
    }
}

export default News;