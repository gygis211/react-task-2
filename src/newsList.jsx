const newsList = [{
    id: 1,
    name: "News 1",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit...",
    data: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    img: "https://sun9-68.userapi.com/impg/E1JHP-ywMtWxdeU7t-9rLf1MjJP9ZESBdlnVSA/Fz5yzUPAQIE.jpg?size=680x383&quality=96&sign=eec468cd782be025f8bc85cfe5aa48a3&type=album"
}, {
    id: 2,
    name: "News 2",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit...",
    data: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    img: "https://sun9-21.userapi.com/impg/gkjrnBPxAmoX_Y0z1CCwzp1IZ9IB9cK3uIvpgw/tdOnoOvv58g.jpg?size=1080x810&quality=96&sign=2cf3bb4d3ad9a1a3a8a6d76ce8f2daa9&type=album"
}, {
    id: 3,
    name: "News 3",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit...",
    data: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    img: "https://sun9-66.userapi.com/impg/tvw7URdfhEGoT92U0y398Jxn02sA6Ayv-ZSWPw/Ey_ngQ_ThwY.jpg?size=978x1080&quality=96&sign=3499168ee8bccb5fab79aab28cd4d459&type=album"
}, {
    id: 4,
    name: "News 4",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit...",
    data: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    img: "https://sun9-45.userapi.com/impg/mzUTEG6XHdQq9rgftITSZt-Wsoev-ENAB_55CA/ZiVJcAHzIv8.jpg?size=600x443&quality=96&sign=9df3e150f6d218d94861a10827051b16&type=album"
}, {
    id: 5,
    name: "News 5",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit...",
    data: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    img: "https://sun9-29.userapi.com/impf/c639720/v639720597/2c02d/qOlM8gfHju8.jpg?size=700x525&quality=96&sign=3b6c86655904d2b8215ea91ccb511438&type=album"
}]

// newsList.reverse();

export default newsList;